#!/bin/sh
app="Brave Browser"
osascript - <<EOF
-- this script will start/activate iTerm (close the default window if the app had been newly started), then open a new session with a desired profile

on is_running(appName)
    tell application "System Events" to (name of processes) contains appName
end is_running

set iTermRunning to is_running("$app")
tell application "$app"
    activate
    if iTermRunning then
        make new window
        # create window with default profile
    end if
end tell
EOF


